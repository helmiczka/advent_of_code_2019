import csv
import sys

def trace_wire(wire):
    start_cell = (0,0)
    current_cell = start_cell
    current_step = 0
    
    wire_set = set()
    step_dict = {}
    
    for segment in wire:
        direction = segment[0]
        length = int(segment[1:])
        for i in range(0,length):
            current_step += 1
            
            if direction == 'R':
                current_cell = (current[0], current[1]+1)
            elif direction == 'L':
                current_cell = (current[0], current[1]-1)
            elif direction == 'U':
                current_cell = (current[0]+1, current[1])
            elif direction == 'D':
                current_cell = (current[0]-1, current[1])
            wire_set.add(current)
            step_dict[current] = current_step
    
    return (wire_set, step_dict)


with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

# print(data)

wire0 = data[0]
wire1 = data[1]

(wire0_set, wire0_dict) = trace_wire(wire0)
(wire1_set, wire1_dict) = trace_wire(wire1)

intersections = wire0_set.intersection(wire1_set)
print(intersections)
for i in intersections:
    print(i, wire0_dict[i], wire1_dict[i])
