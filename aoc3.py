import csv
import sys

def draw_wire(wire):
    start = (0,0)
    current = start
    
    wire_set = set()
    
    for segment in wire:
        direction = segment[0]
        length = int(segment[1:])
        for i in range(0,length):
            if direction == 'R':
                current = (current[0], current[1]+1)
            elif direction == 'L':
                current = (current[0], current[1]-1)
            elif direction == 'U':
                current = (current[0]+1, current[1])
            elif direction == 'D':
                current = (current[0]-1, current[1])
            wire_set.add(current)
    
    return wire_set


with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

# print(data)

wire0 = data[0]
wire1 = data[1]

wire0_set = draw_wire(wire0)
wire1_set = draw_wire(wire1)

intersections = wire0_set.intersection(wire1_set)
print(intersections)
