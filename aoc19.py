import csv
import sys
from enum import Enum
from classy_intcode import IntcodeMachine


with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

tape = data[0]

affected_num = 0
beam = dict()

print('Drone starts')

for y in range(0, 50):
    for x in range(0, 50):
        # get output from the drone
        tapecopy = tape.copy()
        (affected, _) = IntcodeMachine(tapecopy).execute_intcode(USER_INPUT=[x,y])
        affected_num += affected[0]
        # print(x,y,affected)
        beam[(x,y)] = affected[0]


print('Drone ends')

# draw beam
for y in range(0, 50):
    for x in range(0, 50):
        if beam[(x,y)] == 0:
            print('.', end='')
        else:
            print('#', end='')
    print()

print('Part 1', affected_num)


# # part 1 and 1/2
# beam = dict()
# print('Drone starts')

# for y in range(10, 50):
    # beam_end_detected = False
    # for x in range(10, 50):
        # # get output from the drone
        # tapecopy = tape.copy()
        # (affected, _) = IntcodeMachine(tapecopy).execute_intcode(USER_INPUT=[x,y])
        # affected_num += affected[0]
        
        # beam[(x,y)] = affected[0]
        
        # if (x - 1, y) in beam and beam[(x - 1, y)] == 1 and affected[0] == 0:
            # # print('beam end',x,y)
            # beam_end_detected = True
        # if beam_end_detected:
            # break

# # draw beam
# for y in range(10, 50):
    # for x in range(10, 50):
        # if (x,y) in beam:
            # if beam[(x,y)] == 0:
                # print('.', end='')
            # else:
                # print('#', end='')
    # print()


print('Drone ends')

y = 900

beam = dict()

# for y in range(0, 2000):
while True:
    y += 1
    x = 0
    beam_end_detected = False
    beam_x_width = 0
    # for x in range(0, 2000):
    while True:
        # get output from the drone
        x += 1
        tapecopy = tape.copy()
        (affected, _) = IntcodeMachine(tapecopy).execute_intcode(USER_INPUT=[x,y])
        affected_num += affected[0]
        
        beam[(x,y)] = affected[0]
        
        if affected[0] == 1:
            beam_x_width += 1
        if (x - 1, y) in beam and beam[(x - 1, y)] == 1 and affected[0] == 0:
            beam_end_detected = True
            # print('Beam x-width',beam_x_width)
        if beam_end_detected or x > 2000:
            break
            
        if (x, y - 99) in beam and beam[(x, y - 99)] == 1 and (x - 99, y) in beam and beam[(x - 99, y)] == 1:
            print('ok stop')
            break
            
    print (x,y)
    if (x, y - 99) in beam and beam[(x, y - 99)] == 1 and (x - 99, y) in beam and beam[(x - 99, y)] == 1:
        print('ok stop')
        break

print(x-99, y-99)
