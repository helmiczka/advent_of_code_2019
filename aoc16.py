import sys


BASE_PATTERN = [0,1,0,-1]


def generate_line(i, size):
    line = []
    j = 0
    while j < size:
        line += [BASE_PATTERN[0]] * i
        line += [BASE_PATTERN[1]] * i
        line += [BASE_PATTERN[2]] * i
        line += [BASE_PATTERN[3]] * i
        j += i*4
        
    return line[1:size+1]


def generate_pattern_table(size):
    table = []
    for i in range(0, size):
        line = generate_line(i+1, size)
        table += [line]
    
    return table


def fft(input, pattern_table):
    new_input = []
    for i in range(0, len(input)):
        products = zip(input, pattern_table[i])
        sum = 0
        for p in products:
            sum += p[0]*p[1]
        number = abs(sum) % 10
        # print(number)
        new_input.append(number)
        
        
    return new_input


def fft2(input, size):
    new_input = []
    for i in range(0, len(input)):
        products = zip(input, generate_line(i +1, size))
        sum = 0
        for p in products:
            sum += p[0]*p[1]
        number = abs(sum) % 10
        # print(number)
        new_input.append(number)
        
        
    return new_input


def sub_fft(input, orig_len):
    length = len(input)
    new_input = fft2(input, length)
    print('orig len', orig_len)
    print('curr len', length)
    print('Sub-input', input)
    if length == orig_len:
        print('returning ',length // 2,'long sub part')
        print('returning',fft2(input, orig_len)[length // 2:])
        return new_input[length // 2:]
    elif length < orig_len:
        return []
    elif length > orig_len:
        print('returning ',length // 2,'long sub part')
        # print('returning',new_input[length // 2 +1:-(length // 4)],'plus the rest')
        
        print(new_input,' cut ',length // 2,' -', -(length // 4))
        print('returning',new_input[length // 2:-(length // 4)],'and the rest')
    
        return new_input[length // 2:-(length // 4)] + sub_fft(input[length -orig_len:], orig_len)


def fft_for_periodic_input(input, orig_len):
    length = len(input)
    tail_len = (len(input) - orig_len) // 2 +1
    print('This part - input length', length, 'tail length', tail_len)
    print('Tail length', tail_len)
    new_input = []
    for i in range(0, length - tail_len +1):
        products = zip(input, generate_line(i+1, length))
        sum = 0
        # print('p0', input)
        for p in products:
            # print('p0',p[0])
            # print('p1',p[1])
            sum += p[0]*p[1]
        number = abs(sum) % 10
        # print(number)
        new_input.append(number)
    print('Computed', new_input)
    
    tail = sub_fft(input[0:-orig_len], orig_len)
    new_input += tail
    print('complete line',new_input)
        
    return new_input

# test 1 
input = [1, 2, 3, 4, 5, 6, 7, 8]
PHASES = 4

pattern_table = generate_pattern_table(len(input))

for i in range(0, PHASES):
    input = fft(input, pattern_table)

assert input == [0,1,0,2,9,4,9,8], 'Simple test from part 1 failed'
# end test 1

# test 2 
input = [1, 2, 3, 4, 5, 6, 7, 8]
input = input
input = input *3
print('input',input)
orig_len = 8
PHASES = 4

input_classic = input.copy()

pattern_table = generate_pattern_table(len(input))

for i in range(0, PHASES):
    input = fft_for_periodic_input(input, orig_len)
    input_classic = fft(input_classic, pattern_table)
    print('new method', input)
    print('old method', input_classic)

print('result',input)
# assert input == [9,9,7,0,4,1,1,4,8,8,2,4,9,4,9,8], 'Simple test from part 2 failed'
# end test 2


PHASES = 10

input = []

f = open(sys.argv[1], "r")
for line in f:
    print(line)
    for digit in line.strip():
        input.append(int(digit))

input = input
input = input * 6

# print(input)

pattern_table = generate_pattern_table(len(input))

# print(pattern_table)

# for i in range(0, PHASES):
    # input = fft(input, pattern_table)
    # print(input[0:7])
    # print('phase',i)

# print(input)

