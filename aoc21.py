import csv
import sys
from classy_intcode import IntcodeMachine


with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

tape = data[0].copy()

three_tile_wide_hole_example_instructions = [ord(x) for x in
    'NOT A J\n'
    'NOT B T\n'
    'AND T J\n'
    'NOT C T\n'
    'AND T J\n'
    'AND D J\n'
    'WALK\n']

one_to_three_tile_wide_hole_instructions = [ord(x) for x in
                'NOT A J\n'
                'NOT B T\n'
                'OR T J\n'
                'NOT C T\n'
                'OR T J\n'
                'AND D J\n'
                'WALK\n']

user_input = one_to_three_tile_wide_hole_instructions
print('Instruction', user_input)
robot_output = IntcodeMachine(tape).execute_intcode(USER_INPUT=user_input)[0]
damage = robot_output[-1]
for char in robot_output[:-1]:
    print( chr(char), end='')
print('Part 1')
print('Damage:',damage)

# Part 2

tape = data[0].copy()

part2_instructions = [ord(x) for x in
                'NOT A J\n'
                'NOT B T\n'
                'OR T J\n'
                'NOT C T\n'
                'OR T J\n'
                'AND D J\n'
                'NOT J T\n' # reset T to False
                'OR E T\n'  # if tile on E, jump
                'OR H T\n'  # or if tile on H, jump
                'AND T J\n' # if tile on E or tile on H, jump
                'RUN\n\n']

user_input = part2_instructions
print('Instruction', user_input)
robot_output = IntcodeMachine(tape).execute_intcode(USER_INPUT=user_input)[0]
damage = robot_output[-1]
for char in robot_output[:-1]:
    print( chr(char), end='')
print('Part 2')
print('Damage:',damage)
