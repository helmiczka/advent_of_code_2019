import csv
import sys


field = dict()
fields = []

f = open(sys.argv[1], "r")

x = 0
y = 0
for line in f:
    print(line)
    for digit in line.strip():
        field[(x,y)] = digit
        x += 1
        if x == 5:
            y += 1
            x = 0


print('Puzzle input:')
for y in range(0,5):
    for x in range(0,5):
        print(field[(x,y)], end='')
    print()


def cycleDetected(fields, field):
    return field in fields


def biodiversity(field):
    result = 0
    multiplication = 1
    for y in range(0,5):
        for x in range(0,5):
            if field[(x, y)] == '#':
                result += multiplication
            multiplication *= 2
    return result


def neighbors(field, x, y):
    neighbors_count = 0
    if (x-1, y) in field and field[(x-1, y)] == '#':
        neighbors_count +=1
    if (x+1, y) in field and field[(x+1, y)] == '#':
        neighbors_count +=1
    if (x, y-1) in field and field[(x, y-1)] == '#':
        neighbors_count +=1
    if (x, y+1) in field and field[(x, y+1)] == '#':
        neighbors_count +=1
    return neighbors_count


def new_minute(field):
    new_field = dict()
    for y in range(0,5):
        for x in range(0,5):
            # print(x,y,neighbors(field,x,y))
            if field[(x, y)] == '.' and neighbors(field, x, y) in range(1,3):
                new_field[(x, y)] = '#'
            elif field[(x, y)] == '#' and neighbors(field, x, y) != 1:
                new_field[(x, y)] = '.'
            else:
                new_field[(x, y)] = field[(x, y)]
    return new_field


while not cycleDetected(fields, field):
    new_field = new_minute(field)
    fields.append(field)
    field = new_field.copy()

print('This starts the cycle:')
for y in range(0,5):
    for x in range(0,5):
        print(field[(x,y)], end='')
    print()

print('Biodiversity rating:',biodiversity(field))

# tape = data[0]
# for char in IntcodeMachine(tape).execute_intcode()[0]:
    # print( chr(char), end='')
