import csv
import sys
from itertools import permutations

a = ['a', 'b', 'c', 'd', 'e', 'f', 'g']

perm_gen = permutations(a)

possible_paths = []

for perm in perm_gen:
    if (perm.index('b') < perm.index('c') and
        perm.index('c') < perm.index('d') and
        perm.index('a') < perm.index('e') and
        perm.index('f') < perm.index('g') and
        perm.index('a') < perm.index('e') and
        perm.index('c') < perm.index('f') and
        perm.index('d') < perm.index('f')):
            print(perm)
            possible_paths.append(perm)

print(len(possible_paths))


a = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p']

perm_gen = permutations(a)

possible_paths = []

for perm in perm_gen:
    if (perm.index('g') < perm.index('i') and
        perm.index('a') < perm.index('j') and
        perm.index('d') < perm.index('o') and
        perm.index('e') < perm.index('k') and
        perm.index('b') < perm.index('n') and
        perm.index('f') < perm.index('l') and
        perm.index('c') < perm.index('m')
        and
        perm.index('c') < perm.index('i') and
        perm.index('e') < perm.index('p') and
        perm.index('b') < perm.index('j') and
        perm.index('f') < perm.index('o') and
        perm.index('a') < perm.index('k') and
        perm.index('g') < perm.index('n') and
        perm.index('h') < perm.index('m') and
        perm.index('d') < perm.index('l')):
            # print(perm)
            possible_paths.append(perm)

print(len(possible_paths))



# with open(sys.argv[1], newline='') as csvfile:
    # data = list(csv.reader(csvfile))

# tape = data[0]
# for char in IntcodeMachine(tape).execute_intcode()[0]:
    # print( chr(char), end='')
