import csv
import sys
from classy_intcode import IntcodeMachine


with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

tape = data[0]

tiles = dict()
current_pos = (0, 0)
user_input = 0
halted = False
game_started = False

block_count = 0

def draw_screen(tiles):
    for y in range(0, 25):
        for x in range(0, 42):
            if (x, y) not in tiles:
                print(' ', end='')
            elif tiles[(x, y)] == 0:
                print(' ', end='')
            elif tiles[(x, y)] == 1:
                print('I', end='')
            elif tiles[(x, y)] == 2:
                print('X', end='')
            elif tiles[(x, y)] == 3:
                print('-', end='')
            elif tiles[(x, y)] == 4:
                print('o', end='')
        print()

im = IntcodeMachine(tape)

while not halted:

    # get output from the robot brain
    (x, halted) = im.execute_intcode(USER_INPUT=user_input, pause_after_output=True)
    if not halted:
        (y, halted) = im.execute_intcode(USER_INPUT=user_input, pause_after_output=True)
    if not halted:
        (tile, halted) = im.execute_intcode(USER_INPUT=user_input, pause_after_output=True)

    # Part 1 - block count
    tiles[(x[0],y[0])] = tile[0]
    if tile[0] == 2:
        block_count +=1
    
    if (x[0] == -1) and (y[0] == 0):
        print('Current score: ', tile[0])
        game_started = True
    
    # user I/O
    # if game_started:
        # draw_screen(tiles)

print('Part 1:',block_count,'blocks')
