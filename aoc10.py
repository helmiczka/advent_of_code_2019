import csv
import sys
import numpy as np
import operator
import math


def compute_vector(target_x, target_y, station_x, station_y):
    vector = (target_x - station_x, target_y - station_y)
    larger_number = max(abs(vector[0]),abs(vector[1]))
    if (larger_number != 0):
        vector = (vector[0]/larger_number, vector[1]/larger_number)
    return vector


# https://stackoverflow.com/a/35134034
def compute_azimuth(north, asteroid, station):
    v0 = np.array(north) - np.array(station)
    v1 = np.array(asteroid) - np.array(station)

    angle = np.math.atan2(np.linalg.det([v0,v1]),np.dot(v0,v1))
    degrees = np.degrees(angle)
    if degrees < 0:
        degrees += 360
    return round(degrees,2)


def is_visible_asteroid(target_x, target_y, station_x, station_y):
    if (target_x == station_x and target_y == station_y):
        return False

    if (space[target_y][target_x] == '#'):
        vector = compute_vector(target_x, target_y, station_x, station_y)
        vector_dict.setdefault(vector, []).append((target_x, target_y))
        return True
    
    return False


def count_visible_asteroids(space, _x, _y, vector_dict):
    count = 0
    if (space[_y][_x] != '#'):
        return count
    
    for x in range((len(space[0]))):
        for y in range(len(space)):
            if is_visible_asteroid(x, y, _x, _y):
                count +=1

    return count


def find_closest_asteroid(asteroids, station_x, station_y):
    closest_length = math.inf
    closest_asteroid = (0,0)
    for asteroid in asteroids:
        length = np.linalg.norm([asteroid[0] - station_x, asteroid[1] - station_y])
        if length < closest_length:
            closest_asteroid = asteroid
    return closest_asteroid


def vaporize(azimuths, station_x, station_y):
    i = 0
    vaped = ()
    for azimuth in sorted(azimuths):
        i += 1
        if len(azimuths[azimuth]) > 1:
            closest = find_closest_asteroid(azimuths[azimuth], station_x, station_y)
            vaped = closest
            azimuths[azimuth].remove(closest)
        else:
            vaped = azimuths[azimuth][0]
        
        if i == 199:
            print('The 199th asteroid to be vaporized is at',vaped)
            
        if i == 200:
            print('The 200th asteroid to be vaporized is at',vaped)
            
        if i == 201:
            print('The 201st asteroid to be vaporized is at',vaped)
            break


space =[]
f = open(sys.argv[1], "r")
for line in f:
    space_slice = []
    for char in line.strip():
        space_slice += char
    space.append(space_slice)
# print(space)
# print(len(space[0]))
# print(len(space))

best_x = 0
best_y = 0
max_visible_asteroids = 0
best_vectors = dict()

for y in range((len(space))):
    for x in range(len(space[0])):
        vector_dict = dict()
        count = count_visible_asteroids(space, x, y, vector_dict)
        count = max(len(vector_dict), 0)
        # print(count,' ', end='')
        if (count > max_visible_asteroids):
            best_x = x
            best_y = y
            max_visible_asteroids = count
            best_vectors = vector_dict.copy()
    # print()

print('Part 1')
print('x', best_x, 'y', best_y, 'visible',max_visible_asteroids)


print('Part 2')

azimuths = dict()
for vector in best_vectors:
    for asteroid in best_vectors[vector]:
        azimuth = compute_azimuth([best_x, 0], [asteroid[0], asteroid[1]], [best_x, best_y])
        azimuths.setdefault(azimuth, []).append((asteroid[0], asteroid[1]))

vaporize(azimuths, best_x, best_y)
