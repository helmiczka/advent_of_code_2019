import sys

def count_indirect_orbits(orbits):
    indirect_orbits = 0
    for center in orbits:
        # print(center)
        objects_to_expand = list(orbits[center].copy())
        
        for orbiting in objects_to_expand:
            if orbiting in orbits:
                indirect_orbits += len(orbits[orbiting])
                objects_to_expand += list(orbits[orbiting])
    # print(indirect_orbits)
    return indirect_orbits

def count_all_orbits(orbits):
    indirect_orbits = count_indirect_orbits(orbits)
    # print(all_orbits)
    count = 0
    for key in orbits:
        # print(key,all_orbits[key])
        count += len(orbits[key]) 
    return count + indirect_orbits

orbits = dict()

f = open(sys.argv[1], "r")
for line in f:
    (center, orbiting) = tuple(line.strip().split(')'))
    orbits.setdefault(center, set()).add(orbiting)

# print(orbits)
print(count_all_orbits(orbits))

