import csv
import sys
from classy_intcode import IntcodeMachine


class Computer:
    addr = 0
    
    def __init__(self, tape, addr):
        self.im = IntcodeMachine(tape)
        self.addr = addr


with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

tape = data[0]

computers = [Computer(tape, x) for x in range (0, 50)]

for computer in computers:
    print(computer.addr)

# while not halted:

    # # get output from the robot brain
    # (x, halted) = im.execute_intcode(USER_INPUT=user_input, pause_after_output=True)
    # if not halted:
        # (y, halted) = im.execute_intcode(USER_INPUT=user_input, pause_after_output=True)
    # if not halted:
        # (tile, halted) = im.execute_intcode(USER_INPUT=user_input, pause_after_output=True)

    # # Part 1 - block count
    # tiles[(x[0],y[0])] = tile[0]
    # if tile[0] == 2:
        # block_count +=1
    
    # if (x[0] == -1) and (y[0] == 0):
        # print('Current score: ', tile[0])
        # game_started = True
    
    # # user I/O
    # # if game_started:
        # # draw_screen(tiles)

# print('Part 1:',block_count,'blocks')
