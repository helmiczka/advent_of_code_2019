import csv
import sys

layer = 0
DIM_X = 25
DIM_Y = 6
pixels = []
layer = []

def find_layer_with_least_zeros(pixels):
    i = 0
    layer_with_least_zeros = 0
    least_count_of_zeros = DIM_X * DIM_Y
    
    for layer in pixels:
        # print(i, ': ', layer.count(0), least_count_of_zeros)
        if layer.count(0) < least_count_of_zeros:
            least_count_of_zeros = layer.count(0)
            layer_with_least_zeros = i
        i += 1
    
    return layer_with_least_zeros 


def get_pixel(layer, x, y):
    return layer[x + DIM_X*y]


def render_image(pixels):
    rendered_image = []
    for y in range(0, DIM_Y):
        for x in range(0, DIM_X):
            line = []
            for layer in pixels:
                pixel = get_pixel(layer, x, y)
                if (pixel == 1):
                    rendered_image.append('X')
                    break
                elif (pixel == 0):
                    rendered_image.append(' ')
                    break
    
    return rendered_image


f = open(sys.argv[1], "r")
file_position = 1
for line in f:
    for digit in line.strip():
      layer.append(int(digit))
      if (file_position % (DIM_X * DIM_Y) == 0):
          pixels.append(layer)
          layer = []
      file_position += 1


i = find_layer_with_least_zeros(pixels)

print('1st part', pixels[i].count(1)*pixels[i].count(2))

rendered_image = render_image(pixels)
print('2st part')

for y in range(0, DIM_Y):
    for x in range(0, DIM_X):
        print(get_pixel(rendered_image, x, y), end ='')
    print()
