import csv
import sys

def decode_instruction(instruction):
    instruction_reversed = str(instruction)[::-1]
    opcode = int(instruction_reversed[0:1])
    modes = [0, 0, 0]
    for i in range(0, len(instruction_reversed[2:])):
        modes[i] = int(instruction_reversed[2+i])
    return(opcode, modes)

PLUS = 1
MULT = 2
INPUT = 3
OUTPUT = 4
HALT = 99

USER_INPUT = 1

with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

tape = data[0]

i = 0

while (i < len(tape) and tape[i] != HALT):
    instruction = tape[i]
    (opcode, modes) = decode_instruction(instruction)
    instruction_length = 1
    
    if (opcode == HALT):
        break
    elif (opcode == PLUS):
        if (modes[0] == 0):
            a = int(tape[int(tape[i+1])])
        else:
            a = int((tape[i+1]))
        if (modes[1] == 0):
            b = int(tape[int(tape[i+2])])
        else:
            b = int((tape[i+2]))
        if (modes[2] == 0):
            result_pointer = int(tape[i+3])
        else:
            result_pointer = i+3
        
        tape[result_pointer] = a + b
        instruction_length = 4
    elif (opcode == MULT):
        if (modes[0] == 0):
            a = int(tape[int(tape[i+1])])
        else:
            a = int((tape[i+1]))
        if (modes[1] == 0):
            b = int(tape[int(tape[i+2])])
        else:
            b = int((tape[i+2]))
        if (modes[2] == 0):
            result_pointer = int(tape[i+3])
        else:
            result_pointer = i+3
        
        tape[result_pointer] = a * b
        instruction_length = 4
    elif (opcode == INPUT):
        tape[int(tape[i+1])] = USER_INPUT
        instruction_length = 2
    elif (opcode == OUTPUT):
        print('MACHINE OUTPUT: ', int(tape[int(tape[i+1])]))
        instruction_length = 2
    else:
        print('Fatal error on instruction ', instruction)
        break
    
    i += instruction_length

print('TAPE DUMP')
print(tape)
