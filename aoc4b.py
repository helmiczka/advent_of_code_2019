def could_be_password(password):
    digits = [int(d) for d in str(password)]
    for i in range(1,len(digits)):
        if digits[i] < digits[i-1]:
            return False
    
    for c in str(password):
        if str(password).count(c) == 2:
            return True
    
    return False


count = sum([1 for p in range(158126, 624574) if could_be_password(p)])

print(count)
