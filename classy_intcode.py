class IntcodeMachine:
    tape = []
    i = 0
    relative_base = 0
    halted = False
    
    def __init__(self, tape):
        self.i = 0
        self.relative_base = 0
        self.halted = False
        self.tape = tape
        
    
    def decode_instruction(instruction):
        instruction_reversed = str(instruction)[::-1]
        opcode = int(str(instruction)[-2:])
        modes = [0, 0, 0]
        for i in range(0, len(instruction_reversed[2:])):
            modes[i] = int(instruction_reversed[2 + i])
        return opcode, modes


    def get_data_pointer(_tape, index, mode, relative_base):
        if index > len(_tape):
            print('Not Enough Memory')
            quit()

        pointer = 0

        if mode == 0:
            pointer = int(_tape[index])
        elif mode == 1:
            pointer = index
        elif mode == 2:
            pointer = relative_base + int(_tape[index])
        
        if (pointer >= len(_tape)):
            _tape.extend([0] * (pointer - len(_tape) + 1))

        return pointer


    PLUS = 1
    MULT = 2
    INPUT = 3
    OUTPUT = 4
    JUMPTRUE = 5
    JUMPFALSE = 6
    LT = 7
    EQ = 8
    SETRBASE = 9
    HALT = 99


    def execute_intcode(self, USER_INPUT=1, pause_after_output=False):
        exitcode = []
        ui_index = 0

        while self.i < len(self.tape):
            instruction = self.tape[self.i]
            (opcode, modes) = IntcodeMachine.decode_instruction(instruction)
            # print('IP', self.i, 'base', self.relative_base,'instruction', instruction, 'modes', modes)
            instruction_length = 0

            if (opcode == IntcodeMachine.HALT):
                # print('Intcode program halted')
                self.halted = True
                break
            else:
                a_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 1, modes[0], self.relative_base)

                if (opcode == IntcodeMachine.PLUS):
                    # print('  operands: ', self.tape[i+1], self.tape[1+2], self.tape[i+3])
                    b_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 2, modes[1], self.relative_base)
                    result_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 3, modes[2], self.relative_base)

                    self.tape[result_pointer] = int(self.tape[a_pointer]) + int(self.tape[b_pointer])

                    # print('  decoded: ', self.tape[a_pointer], self.tape[b_pointer], self.tape[result_pointer],'=>',result_pointer)
                    instruction_length = 4

                elif (opcode == IntcodeMachine.MULT):
                    # print('  operands: ', self.tape[self.i+1], self.tape[self.i+2], self.tape[self.i+3])
                    b_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 2, modes[1], self.relative_base)
                    result_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 3, modes[2], self.relative_base)

                    self.tape[result_pointer] = int(self.tape[a_pointer]) * int(self.tape[b_pointer])

                    # print('  decoded: ', self.tape[a_pointer], '*', self.tape[b_pointer], '=', self.tape[result_pointer],'=>',result_pointer)
                    instruction_length = 4

                elif (opcode == IntcodeMachine.INPUT):
                    if isinstance(USER_INPUT, list):
                        input = USER_INPUT[ui_index]
                        ui_index += 1
                    else:
                        input = USER_INPUT
                    # print('MACHINE INPUT: ', input)
                    # print('  decoded: ', self.tape[a_pointer],'=>',a_pointer)
                    self.tape[a_pointer] = input
                    # print('  written: ', self.tape[a_pointer])
                    instruction_length = 2

                elif (opcode == IntcodeMachine.OUTPUT):
                    exitcode.append(int(self.tape[a_pointer]))
                    # print('MACHINE OUTPUT: ', exitcode)
                    instruction_length = 2
                    if pause_after_output:
                        # save machine state
                        self.i += instruction_length
                        break

                elif (opcode == IntcodeMachine.JUMPTRUE):
                    # print('  operands: ', self.tape[i+1], self.tape[i+2])
                    b_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 2, modes[1], self.relative_base)

                    dest_pointer = int(self.tape[b_pointer])

                    # print('  decoded: ', self.tape[a_pointer],'=>', dest_pointer)

                    if (int(self.tape[a_pointer]) != 0):
                        self.i = dest_pointer
                        instruction_length = 0
                    else:
                        instruction_length = 3

                elif (opcode == IntcodeMachine.JUMPFALSE):
                    # print('  operands: ', self.tape[i+1], self.tape[i+2])
                    b_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 2, modes[1], self.relative_base)

                    dest_pointer = int(self.tape[b_pointer])

                    # print('  decoded: ', self.tape[a_pointer], dest_pointer)

                    if (int(self.tape[a_pointer]) == 0):
                        self.i = dest_pointer
                        instruction_length = 0
                    else:
                        instruction_length = 3

                elif (opcode == IntcodeMachine.LT):
                    # print('  operands: ', self.tape[i+1], self.tape[1+2], self.tape[i+3])
                    b_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 2, modes[1], self.relative_base)
                    result_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 3, modes[2], self.relative_base)

                    # print('  decoded: ', self.tape[a_pointer], self.tape[b_pointer], self.tape[result_pointer])

                    if (int(self.tape[a_pointer]) < int(self.tape[b_pointer])):
                        self.tape[result_pointer] = 1
                    else:
                        self.tape[result_pointer] = 0

                    # print('  result: ', result_pointer ,'<=', self.tape[result_pointer])

                    instruction_length = 4

                elif (opcode == IntcodeMachine.EQ):
                    # print('  operands: ', self.tape[i+1], self.tape[1+2], self.tape[i+3])
                    b_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 2, modes[1], self.relative_base)
                    result_pointer = IntcodeMachine.get_data_pointer(self.tape, self.i + 3, modes[2], self.relative_base)

                    if (int(self.tape[a_pointer]) == int(self.tape[b_pointer])):
                        self.tape[result_pointer] = 1
                    else:
                        self.tape[result_pointer] = 0

                    # print('  decoded: ', self.tape[a_pointer], self.tape[b_pointer], self.tape[result_pointer])

                    instruction_length = 4

                elif (opcode == IntcodeMachine.SETRBASE):
                    # print('  operands: ', self.tape[a_pointer], '  decoded: ', self.tape[a_pointer])

                    adjustment = int(self.tape[a_pointer])
                    # print('  adjusting by',adjustment)
                    self.relative_base += adjustment
                    instruction_length = 2

                else:
                    print('Fatal error on instruction ', instruction, ' at IP ', self.i)
                    self.halted = True
                    break

            self.i += instruction_length

        # print('TAPE DUMP')
        # print(self.tape)

        if pause_after_output and exitcode == []:
            exitcode = [0]
        return (exitcode, self.halted)
