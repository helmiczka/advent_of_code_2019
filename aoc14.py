import sys
import re
import itertools
import numpy
import math


STEPS = 1000


class Equation:
    result = ()
    inputs = []
    
    def __init__(self, _result, _inputs):
        self.result = _result
        self.inputs = _inputs


def read_equation(line):
    inputs, result = line.strip().split(' => ')
    result = tuple(result.split())
    inputs = [tuple(input.split()) for input in (inputs.split(', '))]
    
    return Equation(result, inputs)


def calculate_required_quantity(given_quantity, product, primal_source, equations):
    quantity_of_known_sources = 0
    for quantity, name in equations:
        if name == product:
            print('Unknown', quantity, name)
            for sources in equations[(quantity, name)]:
                new_quantity = sources[0]
                new_product = sources[1]
                print('Recursing for', new_product)
                if new_product == primal_source:
                    print('returning', new_quantity)
                    print()
                    
                    print('Got',new_quantity, new_product, 'required for', int(quantity),name)
                    quantity_of_known_sources = numpy.gcd(int(new_quantity), int(quantity))
                else:
                    quantity_of_known_sources += int(new_quantity) * calculate_required_quantity(
                                            new_quantity, new_product, 'ORE', equations)
    print ('return formula',quantity_of_known_sources, int(quantity), given_quantity)
    return quantity_of_known_sources  * given_quantity

first_level = dict()
first_level_elements = []
middle_level = dict()
fuel = dict()

f = open(sys.argv[1], "r")
for line in f:
    equation = read_equation(line)
    # print (equation.inputs)
    if equation.result[1] == 'FUEL':
        fuel[equation.result] = equation.inputs
    elif len(equation.inputs[0]) == 2 and equation.inputs[0][1] == 'ORE':
        first_level[equation.result] = equation.inputs
        first_level_elements.append(equation.result[1])
    else:
        middle_level[equation.result] = equation.inputs



print('First')
print(first_level)
print(first_level_elements)
print('Midlle')
print(middle_level)
print('FUEL')
print(fuel)

def expanded_to_first_level(fuel, middle_level, first_level_elements):
    print (fuel[list(fuel)[0]])
    for source in fuel[list(fuel)[0]]:
        if source[1] not in first_level_elements:
            return False
    return True


def expand(fuel, middle_level, first_level_elements):
    print(fuel)
    for source in fuel[list(fuel)[0]]:
        # find source in middle_level TODO
        if source in middle_level:
            print('yes', source)
            fuel[list(fuel)[0]].remove(source)
            for part in middle_level[source]:
                fuel[list(fuel)[0]].append(part)
        else:
            for equation in middle_level:
                if source[1] == equation[1]:
                    print('Need',source, 'have formula for' , equation)
                    # if we need less than minimal increment
                    if int(source[0]) < int(equation[0]):
                        fuel[list(fuel)[0]].remove(source)
                        for part in middle_level[equation]:
                            found = False
                            print('Before loop',fuel[list(fuel)[0]])
                            for already_expanded in fuel[list(fuel)[0]]:
                                if already_expanded[1] == part[1]:
                                    print(part,'is already there! ', already_expanded)
                                    fuel[list(fuel)[0]].remove(already_expanded)
                                    fuel[list(fuel)[0]].append((int(already_expanded[0]) + int(part[0]), part[1]))
                                    print('Modified to',(int(already_expanded[0]) + int(part[0]), part[1]))
                                    found = True
                                    break
                            print('After loop',fuel[list(fuel)[0]])
                            if not found:
                                fuel[list(fuel)[0]].append(part)
                            print('Adding',part)
                    # if we need more than minimal increment - we have to multiply
                    else:
                        fuel[list(fuel)[0]].remove(source)
                        for part in middle_level[equation]:
                            found = False
                            print('Before loop',fuel[list(fuel)[0]])
                            need = numpy.ceil((int(source[0])) / int(equation[0]))
                            
                            for already_expanded in fuel[list(fuel)[0]]:
                                if already_expanded[1] == part[1]:
                                    print(part,'is already there! ', already_expanded)
                                    fuel[list(fuel)[0]].remove(already_expanded)
                                    fuel[list(fuel)[0]].append((int(already_expanded[0]) + need*int(part[0]), part[1]))
                                    print('Modified to',(int(already_expanded[0]) + need*int(part[0])), part[1])
                                    found = True
                                    break
                            
                            print('After loop',fuel[list(fuel)[0]])
                            if not found:
                                fuel[list(fuel)[0]].append((need*int(part[0]),part[1]))
                            print('Adding',need*int(part[0]),part[1])
                        
        
        print('Fuel',fuel)
        # replace
        pass


def calculate_ore(fuel, first_level):
    result = 0
    first_level_ammounts = dict()
    for source in fuel[list(fuel)[0]]:
        # find source in first_level
        # note required ammount
        if source[1] in first_level_ammounts:
            first_level_ammounts[source[1]] += int(source[0])
        else:
            first_level_ammounts[source[1]] = int(source[0])
    print('1st level',first_level_ammounts)

    for required_element in first_level_ammounts:
        for equation in first_level:
            # print(required_element, equation[1])
            if required_element == equation[1]:
                print('Transforming ORE to',required_element)
                print('Need',first_level_ammounts[required_element],'of',required_element)
                increment = int(equation[0])
                ammount = 0
                how_many_times = 0
                while ammount < first_level_ammounts[required_element]:
                    ammount += increment
                    how_many_times += 1
                # print('ammount',ammount,'for',first_level_ammounts[required_element],'of',required_element)
                print('But can only produce',ammount)
                print('The transformation has to occur', how_many_times,'times')
                ore = how_many_times * int(first_level[equation][0][0])
                print('ORE neeeded to do this is ',ore,'because',first_level[equation][0][0],'x',how_many_times)
                # print('have this equiation', first_level[equation][0][0])
                result += ore
    
    return result



while not expanded_to_first_level(fuel, middle_level, first_level_elements):
    expand(fuel, middle_level, first_level_elements)
    # break

required_ore = calculate_ore(fuel, first_level)
print(required_ore)

# print(calculate_required_quantity(1, 'FUEL', 'ORE', equations))
# print('Need', calculate_required_quantity(10, 'A', 'ORE', equations), 'ORE for',10, 'A')
# print('Need', calculate_required_quantity(20, 'A', 'ORE', equations), 'ORE for',20, 'A')
# print('Need', calculate_required_quantity(1, 'B', 'ORE', equations), 'ORE for',1, 'B')
# print('Need', calculate_required_quantity(10, 'B', 'ORE', equations), 'ORE for',10, 'B')
# # for p in positions:
    # p.print_vec()
# print()
