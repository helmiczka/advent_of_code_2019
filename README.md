Solved
======

X == both stars  
x == single star  
_ == no star  

    1  2  3  4  5  6  7  8  9  10 11 12 13
    X  X  X  X  X  X  X  X  X  X  X  X  X

    14 15 16 17 18 19 20 21 22 23 24 25
    _  _  x  X  _  X  _  X  x  _  x  _
