def could_be_password(password):
    has_double = False
    digits = [int(d) for d in str(password)]
    for i in range(1,len(digits)):
        if digits[i] == digits[i-1]:
            has_double = True
        elif digits[i] > digits[i-1]:
            pass
        else:
            return False
    
    if has_double:
        return True


count = sum([1 for p in range(158126, 624574) if could_be_password(p)])

print(count)
