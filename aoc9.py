import csv
import sys
from classy_intcode import IntcodeMachine


def run_tests():
    USER_INPUT = 1
    print('Start tests')
    # Day 5 original tests
    assert( IntcodeMachine([3,9,8,9,10,9,4,9,99,-1,8]).execute_intcode()[0] == [0] )
    assert( IntcodeMachine([3,9,7,9,10,9,4,9,99,-1,8]).execute_intcode()[0] == [1] )
    assert( IntcodeMachine([3,3,1108,-1,8,3,4,3,99]).execute_intcode()[0] == [0] )
    assert( IntcodeMachine([3,3,1107,-1,8,3,4,3,99]).execute_intcode()[0] == [1] )
    # Day 9 original tests
    assert( IntcodeMachine([109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]).execute_intcode()[0] == [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99] )
    assert( IntcodeMachine([1102,34915192,34915192,7,4,7,99,0]).execute_intcode()[0] == [1219070632396864] )
    assert( IntcodeMachine([104,1125899906842624,99]).execute_intcode()[0] == [1125899906842624] )
    # Day 9 my own tests
    assert( IntcodeMachine([109,19,99]).execute_intcode()[0] == [] )
    assert( IntcodeMachine([204,1,204,2,99]).execute_intcode()[0] == [1,204] )
    assert( IntcodeMachine([104,2,104,1,99]).execute_intcode()[0] == [2,1] )
    assert( IntcodeMachine([109,3,204,-1,99]).execute_intcode()[0] == [204] )
    assert( IntcodeMachine([105,1,8,104,1,104,2,99,5]).execute_intcode()[0] == [2] )
    assert( IntcodeMachine([205,1,8,104,1,104,2,99,5]).execute_intcode()[0] == [2] )
    assert( IntcodeMachine([109,3,2105,1,7,104,1,104,2,99,7]).execute_intcode()[0] == [2] )
    assert( IntcodeMachine([109,3,1205,1,7,104,1,104,2,99]).execute_intcode()[0] == [2] )
    assert( IntcodeMachine([109,3,2105,0,7,104,1,104,2,99,7]).execute_intcode()[0] == [1,2] )
    assert( IntcodeMachine([106,0,8,104,1,104,2,99,5]).execute_intcode()[0] == [2] )
    assert( IntcodeMachine([109,3,206,0,10,104,1,104,2,99,7]).execute_intcode()[0] == [2] )
    assert( IntcodeMachine([109,2,2206,0,8,104,1,104,2,99,7]).execute_intcode()[0] == [1,2] )
    assert( IntcodeMachine([109,2,2106,0,8,104,1,104,2,99,5]).execute_intcode()[0] == [1,2] )
    assert( IntcodeMachine([109,2,2106,0,8,104,1,104,2,99,7]).execute_intcode()[0] == [2] )
    assert( IntcodeMachine([109,3,206,-3,7,204,1,104,2,99]).execute_intcode()[0] == [7,2] )
    assert( IntcodeMachine([1101,40,60,5,104,1,99]).execute_intcode()[0] == [100] )
    assert( IntcodeMachine([2101,40,1,5,104,1,99]).execute_intcode()[0] == [80] )
    assert( IntcodeMachine([1201,2,40,5,104,1,99]).execute_intcode()[0] == [80] )
    assert( IntcodeMachine([1201,2,40,7,204,7,99]).execute_intcode()[0] == [80] )
    assert( IntcodeMachine([1202,2,40,7,204,7,99]).execute_intcode()[0] == [1600] )
    assert( IntcodeMachine([2102,40,1,7,204,7,99]).execute_intcode()[0] == [1600] )
    assert( IntcodeMachine([1107,100,101,5,104,7,99]).execute_intcode()[0] == [1] )
    assert( IntcodeMachine([1107,101,100,5,104,7,99]).execute_intcode()[0] == [0] )
    assert( IntcodeMachine([1107,100,101,0,204,0,99]).execute_intcode()[0] == [1] )
    assert( IntcodeMachine([1107,101,100,0,204,0,99]).execute_intcode()[0] == [0] )
    assert( IntcodeMachine([1108,100,101,5,104,7,99]).execute_intcode()[0] == [0] )
    assert( IntcodeMachine([1108,101,100,5,104,7,99]).execute_intcode()[0] == [0] )
    assert( IntcodeMachine([1108,100,100,5,104,7,99]).execute_intcode()[0] == [1] )
    assert( IntcodeMachine([1108,100,100,5,104,7,99]).execute_intcode()[0] == [1] )
    assert( IntcodeMachine([109,5,1108,100,100,3,204,-2,99]).execute_intcode()[0] == [1] )
    assert( IntcodeMachine([109,5,1108,100,100,3,204,-2,99]).execute_intcode()[0] == [1] )
    assert( IntcodeMachine([209,2,204,-203,99]).execute_intcode()[0] == [2] )
    assert( IntcodeMachine([203,1,204,1,99]).execute_intcode()[0] == [USER_INPUT] )
    assert( IntcodeMachine([109,1,203,1,204,1,99]).execute_intcode()[0] == [USER_INPUT] )
    print('End tests\n')


run_tests()

with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

tape = data[0]
print( IntcodeMachine(tape).execute_intcode()[0])
# Part 2 is just different user input
print( IntcodeMachine(tape).execute_intcode(USER_INPUT=2)[0])
