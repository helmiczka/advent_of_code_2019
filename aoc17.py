import csv
import sys
from classy_intcode import IntcodeMachine


with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

tape = data[0].copy()
for char in IntcodeMachine(tape).execute_intcode()[0]:
    print( chr(char), end='')

print('Part 2')

tape = data[0].copy()
tape[0] = 2
main_routines = [ord(x) for x in 'A,B,B,A,B,C,A,C,B,C\n']
routine_A = [ord(x) for x in 'L,4,L,6,L,8,L,12\n']
routine_B = [ord(x) for x in 'L,8,R,12,L,12\n']
routine_C = [ord(x) for x in 'R,12,L,6,L,6,L,8\n']
video_feed = [ord(x) for x in 'n\n']
user_input = main_routines + routine_A + routine_B + routine_C + video_feed
print('Main routines', main_routines)
print('Whole input', user_input)
robot_output = IntcodeMachine(tape).execute_intcode(USER_INPUT=user_input)[0]
dust = robot_output[-1]
for char in robot_output[:-1]:
    print( chr(char), end='')
print('Collected dust:',dust, end='')
