import csv
import sys


f = open(sys.argv[1], "r")


def deal_into_new_stack(deck):
    return list(reversed(deck))


def cut_n(deck, n):
    return deck[n:] + deck[:n]


def deal_with_increment(deck, n):
    new_deck = [None] * len(deck)
    i = 0
    for card in deck:
        new_deck[i] = card
        i += n
        if i >= len(deck):
            i -= len(deck)
    return new_deck

factory_deck = [x for x in range(0, 10007)]
test_deck = [x for x in range(0, 10)]

deck = factory_deck

print('deck\n', deck)

for line in f:
    print(line)
    if "deal into new stack" in line:
        deck = deal_into_new_stack(deck)
        print('ins', deck)
    elif "deal with increment" in line:
        n = int(line.strip()[line.rfind(' '):])
        deck = deal_with_increment(deck, n)
        print('win', deck)
    elif "cut" in line:
        n = int(line.strip()[line.rfind(' '):])
        deck = cut_n(deck, n)
        print('cut', deck)

if len(deck) == len(factory_deck):
    print(deck.index(2019))

