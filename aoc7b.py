import csv
import sys
import itertools

def decode_instruction(instruction):
    instruction_reversed = str(instruction)[::-1]
    opcode = int(str(instruction)[-2:])
    modes = [0, 0, 0]
    for i in range(0, len(instruction_reversed[2:])):
        modes[i] = int(instruction_reversed[2+i])
    return(opcode, modes)


def get_data_pointer(tape, index, mode):
    pointer = 0
    
    if (mode == 0):
        if (index < len(tape)):
            pointer = int(tape[index])
    else:
        pointer = index
    
    return pointer


def execute_intcode(tape, user_input_array, i=0):
    exitcode = 0
    user_input_index = 0
    saved_i = 0
    halted = False
    
    while (i < len(tape)):
        instruction = tape[i]
        (opcode, modes) = decode_instruction(instruction)
        instruction_length = 1
        
        if (opcode == HALT):
            print('Intcode program halted')
            halted = True
            break
        else:
            a_pointer = get_data_pointer(tape, i+1, modes[0])
            b_pointer = get_data_pointer(tape, i+2, modes[1])
            result_pointer = get_data_pointer(tape, i+3, modes[2])
            
            if (opcode == PLUS):
                tape[result_pointer] = int(tape[a_pointer]) + int(tape[b_pointer])
                instruction_length = 4
                
            elif (opcode == MULT):
                tape[result_pointer] = int(tape[a_pointer]) * int(tape[b_pointer])
                instruction_length = 4
                
            elif (opcode == INPUT):
                tape[int(tape[i+1])] = user_input_array[user_input_index]
                user_input_index += 1
                instruction_length = 2
                
            elif (opcode == OUTPUT):
                exitcode = tape[int(tape[i+1])]
                print('MACHINE OUTPUT: ', exitcode)
                instruction_length = 2
                saved_i = i+instruction_length
                break
                
            elif (opcode == JUMPTRUE):
                if (int(tape[a_pointer]) != 0):
                    i = int(tape[b_pointer])
                    instruction_length = 0
                else:
                    instruction_length = 3
                    
            elif (opcode == JUMPFALSE):
                if (int(tape[a_pointer]) == 0):
                    i = int(tape[b_pointer])
                    instruction_length = 0
                else:
                    instruction_length = 3
                
            elif (opcode == LT):
                if int(tape[a_pointer]) < int(tape[b_pointer]):
                    tape[result_pointer] = 1
                else:
                    tape[result_pointer] = 0
                
                instruction_length = 4
                
            elif (opcode == EQ):
                if int(tape[a_pointer]) == int(tape[b_pointer]):
                    tape[result_pointer] = 1
                else:
                    tape[result_pointer] = 0
                
                instruction_length = 4
                
            else:
                print('Fatal error on instruction ', instruction, ' at IP ', i)
                break
        
        i += instruction_length

    # print('TAPE DUMP')
    # print(tape)
    
    return (exitcode, tape.copy(), saved_i, halted)

PLUS = 1
MULT = 2
INPUT = 3
OUTPUT = 4
JUMPTRUE = 5
JUMPFALSE = 6
LT = 7
EQ = 8
HALT = 99


with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

tape = data[0]

amp_sequences = list(itertools.permutations(range(5,10)))
max_signal = 0

for sequence in amp_sequences:
    ampE_output = 0
    saved_i_A = 0
    saved_i_B = 0
    saved_i_C = 0
    saved_i_D = 0
    saved_i_E = 0
    saved_tape_A = []
    saved_tape_B = []
    saved_tape_C = []
    saved_tape_D = []
    saved_tape_E = []
    halted = False
    # First run
    (ampA_output, saved_tape_A, saved_i_A, halted) = execute_intcode(tape, [sequence[0], ampE_output])
    (ampB_output, saved_tape_B, saved_i_B, halted) = execute_intcode(tape, [sequence[1], ampA_output])
    (ampC_output, saved_tape_C, saved_i_C, halted) = execute_intcode(tape, [sequence[2], ampB_output])
    (ampD_output, saved_tape_D, saved_i_D, halted) = execute_intcode(tape, [sequence[3], ampC_output])
    (ampE_output, saved_tape_E, saved_i_E, halted) = execute_intcode(tape, [sequence[4], ampD_output])
    # next runs
    while not halted:
        if not halted:
            (ampA_output, saved_tape_A, saved_i_A, halted) = execute_intcode(saved_tape_A, [ampE_output], i=saved_i_A)
        if not halted:
            (ampB_output, saved_tape_B, saved_i_B, halted) = execute_intcode(saved_tape_B, [ampA_output], i=saved_i_B)
        if not halted:
            (ampC_output, saved_tape_C, saved_i_C, halted) = execute_intcode(saved_tape_C, [ampB_output], i=saved_i_C)
        if not halted:
            (ampD_output, saved_tape_D, saved_i_D, halted) = execute_intcode(saved_tape_D, [ampC_output], i=saved_i_D)
        if not halted:
            (ampE_output, saved_tape_E, saved_i_E, halted) = execute_intcode(saved_tape_E, [ampD_output], i=saved_i_E)
    if (ampE_output > max_signal):
        max_signal = ampE_output

print('Max thrust', max_signal)
