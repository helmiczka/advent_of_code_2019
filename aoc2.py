import csv
import sys

PLUS = 1
MULT = 2
HALT = 99

with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

print(data)

tape = data[0]

i = 0

while (i < len(tape) and tape[i] != HALT):
    instruction = int(tape[i])
    print(instruction)
    
    if (instruction  == HALT):
        break
    elif (instruction  == PLUS):
        tape[int(tape[i+3])] = int(tape[int(tape[i+1])]) + int(tape[int(tape[i+2])])
    elif (instruction  == MULT):
        tape[int(tape[i+3])] = int(tape[int(tape[i+1])]) * int(tape[int(tape[i+2])])
    else:
        print('Fatal error')
        break
    
    i += 4
    pass

print(tape)
