def decode_instruction(instruction):
    instruction_reversed = str(instruction)[::-1]
    opcode = int(str(instruction)[-2:])
    modes = [0, 0, 0]
    for i in range(0, len(instruction_reversed[2:])):
        modes[i] = int(instruction_reversed[2 + i])
    return opcode, modes


def get_data_pointer(_tape, index, mode, RELATIVE_BASE):
    if index > len(_tape):
        print('Not Enough Memory')
        quit()

    pointer = 0

    if mode == 0:
        pointer = int(_tape[index])
    elif mode == 1:
        pointer = index
    elif mode == 2:
        pointer = RELATIVE_BASE + int(_tape[index])
    
    if (pointer >= len(_tape)):
        _tape.extend([0] * (pointer - len(_tape) + 1))

    return pointer


PLUS = 1
MULT = 2
INPUT = 3
OUTPUT = 4
JUMPTRUE = 5
JUMPFALSE = 6
LT = 7
EQ = 8
SETRBASE = 9
HALT = 99


def execute_intcode(tape, USER_INPUT=1, i=0, RELATIVE_BASE=0):
    exitcode = 0
    saved_i = i
    halted = False

    while i < len(tape):
        instruction = tape[i]
        (opcode, modes) = decode_instruction(instruction)
        # print('IP',i, 'base', RELATIVE_BASE,'instruction', instruction, 'modes', modes)
        instruction_length = 0

        if (opcode == HALT):
            print('Intcode program halted')
            halted = True
            break
        else:
            a_pointer = get_data_pointer(tape, i + 1, modes[0], RELATIVE_BASE)

            if (opcode == PLUS):
                # print('  operands: ', tape[i+1], tape[1+2], tape[i+3])
                b_pointer = get_data_pointer(tape, i + 2, modes[1], RELATIVE_BASE)
                result_pointer = get_data_pointer(tape, i + 3, modes[2], RELATIVE_BASE)

                tape[result_pointer] = int(tape[a_pointer]) + int(tape[b_pointer])

                # print('  decoded: ', tape[a_pointer], tape[b_pointer], tape[result_pointer],'=>',result_pointer)
                instruction_length = 4

            elif (opcode == MULT):
                # print('  operands: ', tape[i+1], tape[1+2], tape[i+3])
                b_pointer = get_data_pointer(tape, i + 2, modes[1], RELATIVE_BASE)
                result_pointer = get_data_pointer(tape, i + 3, modes[2], RELATIVE_BASE)

                tape[result_pointer] = int(tape[a_pointer]) * int(tape[b_pointer])

                # print('  decoded: ', tape[a_pointer], tape[b_pointer], tape[result_pointer],'=>',result_pointer)
                instruction_length = 4

            elif (opcode == INPUT):
                input = USER_INPUT
                # print('MACHINE INPUT: ', input)
                # print('  decoded: ', tape[a_pointer],'=>',a_pointer)
                tape[a_pointer] = input
                # print('  written: ', tape[a_pointer])
                instruction_length = 2

            elif (opcode == OUTPUT):
                exitcode = int(tape[a_pointer])
                # print('MACHINE OUTPUT: ', exitcode)
                instruction_length = 2
                saved_i = i + instruction_length
                break

            elif (opcode == JUMPTRUE):
                # print('  operands: ', tape[i+1], tape[i+2])
                b_pointer = get_data_pointer(tape, i + 2, modes[1], RELATIVE_BASE)

                dest_pointer = int(tape[b_pointer])

                # print('  decoded: ', tape[a_pointer],'=>', dest_pointer)

                if (int(tape[a_pointer]) != 0):
                    i = dest_pointer
                    instruction_length = 0
                else:
                    instruction_length = 3

            elif (opcode == JUMPFALSE):
                # print('  operands: ', tape[i+1], tape[i+2])
                b_pointer = get_data_pointer(tape, i + 2, modes[1], RELATIVE_BASE)

                dest_pointer = int(tape[b_pointer])

                # print('  decoded: ', tape[a_pointer], dest_pointer)

                if (int(tape[a_pointer]) == 0):
                    i = dest_pointer
                    instruction_length = 0
                else:
                    instruction_length = 3

            elif (opcode == LT):
                # print('  operands: ', tape[i+1], tape[1+2], tape[i+3])
                b_pointer = get_data_pointer(tape, i + 2, modes[1], RELATIVE_BASE)
                result_pointer = get_data_pointer(tape, i + 3, modes[2], RELATIVE_BASE)

                # print('  decoded: ', tape[a_pointer], tape[b_pointer], tape[result_pointer])

                if (int(tape[a_pointer]) < int(tape[b_pointer])):
                    tape[result_pointer] = 1
                else:
                    tape[result_pointer] = 0

                # print('  result: ', result_pointer ,'<=', tape[result_pointer])

                instruction_length = 4

            elif (opcode == EQ):
                # print('  operands: ', tape[i+1], tape[1+2], tape[i+3])
                b_pointer = get_data_pointer(tape, i + 2, modes[1], RELATIVE_BASE)
                result_pointer = get_data_pointer(tape, i + 3, modes[2], RELATIVE_BASE)

                if (int(tape[a_pointer]) == int(tape[b_pointer])):
                    tape[result_pointer] = 1
                else:
                    tape[result_pointer] = 0

                # print('  decoded: ', tape[a_pointer], tape[b_pointer], tape[result_pointer])

                instruction_length = 4

            elif (opcode == SETRBASE):
                # print('  operands: ', tape[a_pointer], '  decoded: ', tape[a_pointer])

                adjustment = int(tape[a_pointer])
                # print('  adjusting by',adjustment)
                RELATIVE_BASE += adjustment
                instruction_length = 2

            else:
                print('Fatal error on instruction ', instruction, ' at IP ', i)
                halted = True
                break

        i += instruction_length

    # print('TAPE DUMP')
    # print(tape)

    return (exitcode, tape.copy(), saved_i, RELATIVE_BASE, halted)
