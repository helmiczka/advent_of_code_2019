import csv
import sys
import itertools

def decode_instruction(instruction):
    instruction_reversed = str(instruction)[::-1]
    opcode = int(str(instruction)[-2:])
    modes = [0, 0, 0]
    for i in range(0, len(instruction_reversed[2:])):
        modes[i] = int(instruction_reversed[2+i])
    return(opcode, modes)


def get_data_pointer(tape, index, mode):
    pointer = 0
    
    if (mode == 0):
        pointer = int(tape[index])
    else:
        pointer = index
    
    return pointer


def execute_intcode(tape, user_input_array):
    exitcode = 0
    i = 0
    user_input_index = 0
    
    while (i < len(tape)):
        instruction = tape[i]
        (opcode, modes) = decode_instruction(instruction)
        instruction_length = 1
        
        if (opcode == HALT):
            # print('Intcode program halted')
            break
        else:
            a_pointer = get_data_pointer(tape, i+1, modes[0])
            b_pointer = get_data_pointer(tape, i+2, modes[1])
            result_pointer = get_data_pointer(tape, i+3, modes[2])
            
            if (opcode == PLUS):
                tape[result_pointer] = int(tape[a_pointer]) + int(tape[b_pointer])
                instruction_length = 4
                
            elif (opcode == MULT):
                tape[result_pointer] = int(tape[a_pointer]) * int(tape[b_pointer])
                instruction_length = 4
                
            elif (opcode == INPUT):
                tape[int(tape[i+1])] = user_input_array[user_input_index]
                user_input_index += 1
                instruction_length = 2
                
            elif (opcode == OUTPUT):
                exitcode = tape[int(tape[i+1])]
                # print('MACHINE OUTPUT: ', exitcode)
                instruction_length = 2
                
            elif (opcode == JUMPTRUE):
                if (int(tape[a_pointer]) != 0):
                    i = int(tape[b_pointer])
                    instruction_length = 0
                else:
                    instruction_length = 3
                    
            elif (opcode == JUMPFALSE):
                if (int(tape[a_pointer]) == 0):
                    i = int(tape[b_pointer])
                    instruction_length = 0
                else:
                    instruction_length = 3
                
            elif (opcode == LT):
                if int(tape[a_pointer]) < int(tape[b_pointer]):
                    tape[result_pointer] = 1
                else:
                    tape[result_pointer] = 0
                
                instruction_length = 4
                
            elif (opcode == EQ):
                if int(tape[a_pointer]) == int(tape[b_pointer]):
                    tape[result_pointer] = 1
                else:
                    tape[result_pointer] = 0
                
                instruction_length = 4
                
            else:
                print('Fatal error on instruction ', instruction, ' at IP ', i)
                break
        
        i += instruction_length

    # print('TAPE DUMP')
    # print(tape)
    
    return exitcode

PLUS = 1
MULT = 2
INPUT = 3
OUTPUT = 4
JUMPTRUE = 5
JUMPFALSE = 6
LT = 7
EQ = 8
HALT = 99

# USER_INPUT = [2, 43]

with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

tape = data[0]

amp_sequences = list(itertools.permutations(range(0,5)))
# print(amp_sequences)

max_signal = 0

for sequence in amp_sequences:
    input_signal = 0
    ampA_output = execute_intcode(tape, [sequence[0], input_signal])
    ampB_output = execute_intcode(tape, [sequence[1], ampA_output])
    ampC_output = execute_intcode(tape, [sequence[2], ampB_output])
    ampD_output = execute_intcode(tape, [sequence[3], ampC_output])
    ampE_output = execute_intcode(tape, [sequence[4], ampD_output])
    if (ampE_output > max_signal):
        max_signal = ampE_output

print(max_signal)
