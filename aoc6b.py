import sys

def create_path(orbits, endpoint):
    path = []
    search_for = endpoint
    while (search_for in orbits):
        path += [orbits[search_for]]
        search_for = orbits[search_for]
    return path[::-1]


def compute_common_prefix_length(path1, path2):
    length = 0
    for length in range(0,(min(len(path1), len(path2)))):
        # print(path1[length], ' ? ' ,path2[length])
        if (path1[length] != path2[length]):
            break
    return length

orbits = dict()

f = open(sys.argv[1], "r")
for line in f:
    (center, orbiting) = tuple(line.strip().split(')'))
    orbits[orbiting] = center

# print(orbits)

you_path = create_path(orbits, 'YOU')
san_path = create_path(orbits, 'SAN')
# print(len(you_path))
# print(you_path)
# print(len(san_path))
# print(san_path)

print('prefix',compute_common_prefix_length(you_path, san_path))
print('result',len(you_path) + len(san_path) - 2*compute_common_prefix_length(you_path, san_path))
