import sys
import re
import itertools
import numpy


STEPS = 1000


class Vector:
    x = 0
    y = 0
    z = 0
    
    def __init__(self, _x=0, _y=0, _z=0):
        self.x = _x
        self.y = _y
        self.z = _z
    
    def size(self):
        return abs(self.x) + abs(self.y) + abs(self.z)
    
    def print_vec(self):
        print(self.x, self.y, self.z)


def read_vector(line):
    x = int(re.search("x=.*, y", line).group()[2:-3])
    y = int(re.search("y=.*, z", line).group()[2:-3])
    z = int(re.search("z=.*>", line).group()[2:-1])
    return Vector(x, y, z)


def apply_gravity(p, v):
    body_pairs = itertools.combinations(range(0, len(p)), 2)
    for bp in body_pairs:
        # print(bp)
        if (p[bp[0]].x > p[bp[1]].x):
            v[bp[0]].x -=1
            v[bp[1]].x +=1
        elif (p[bp[0]].x < p[bp[1]].x):
            v[bp[0]].x +=1
            v[bp[1]].x -=1
        if (p[bp[0]].y > p[bp[1]].y):
            v[bp[0]].y -=1
            v[bp[1]].y +=1
        elif (p[bp[0]].y < p[bp[1]].y):
            v[bp[0]].y +=1
            v[bp[1]].y -=1
        if (p[bp[0]].z > p[bp[1]].z):
            v[bp[0]].z -=1
            v[bp[1]].z +=1
        elif (p[bp[0]].z < p[bp[1]].z):
            v[bp[0]].z +=1
            v[bp[1]].z -=1


def apply_velocity(p, v):
    for pos, vel in zip(p,v):
        pos.x += vel.x
        pos.y += vel.y
        pos.z += vel.z


def total_energy(p, v):
    total_energy = 0
    for pos, vel in zip(p,v):
        potential_energy = pos.size()
        kinetic_energy = vel.size()
        total_energy += potential_energy * kinetic_energy
    return total_energy


def original_state_reached(positions, initial_p):
    for state in zip(positions, initial_p):
        if (state[0].x != state[1].x) or (state[0].y != state[1].y) or (state[0].z != state[1].z):
                return False
        
    return True


positions = []
velocities = []

f = open(sys.argv[1], "r")
for line in f:
    positions.append(read_vector(line))
    velocities.append(Vector())


# for p in positions:
    # p.print_vec()
# print()

for s in range (0, STEPS):
    apply_gravity(positions, velocities)
    apply_velocity(positions, velocities)
    total_energy(positions, velocities)

print('Part 1')
print(total_energy(positions, velocities))

# Part 2

def estimate_period(samples):
    # for i in range(1, int(len(samples)/2)):
        # pattern_to_try = samples[0:i]*2
        # if samples.find(pattern_to_try, 1, -1) != -1:
            # return pattern_to_try.count('x')/2
    # return 0
    
    return samples[0:samples.find(samples[0:100], 1, -1)].count('x')
    

positions = []
velocities = []

p0x_samples = ""
p0y_samples = ""
p0z_samples = ""
p1x_samples = ""
p1y_samples = ""
p1z_samples = ""
p2x_samples = ""
p2y_samples = ""
p2z_samples = ""
p3x_samples = ""
p3y_samples = ""
p3z_samples = ""

v0x_samples = ""
v0y_samples = ""
v0z_samples = ""
v1x_samples = ""
v1y_samples = ""
v1z_samples = ""
v2x_samples = ""
v2y_samples = ""
v2z_samples = ""
v3x_samples = ""
v3y_samples = ""
v3z_samples = ""

f = open(sys.argv[1], "r")
file_position = 1
for line in f:
    positions.append(read_vector(line))
    velocities.append(Vector())


for s in range (0, 500000):
    apply_gravity(positions, velocities)
    apply_velocity(positions, velocities)
    
    p0x_samples+=('x'+str(positions[0].x))
    p0y_samples+=('x'+str(positions[0].y))
    p0z_samples+=('x'+str(positions[0].z))
    
    p1x_samples+=('x'+str(positions[1].x))
    p1y_samples+=('x'+str(positions[1].y))
    p1z_samples+=('x'+str(positions[1].z))
    
    p2x_samples+=('x'+str(positions[2].x))
    p2y_samples+=('x'+str(positions[2].y))
    p2z_samples+=('x'+str(positions[2].z))
    
    p3x_samples+=('x'+str(positions[3].x))
    p3y_samples+=('x'+str(positions[3].y))
    p3z_samples+=('x'+str(positions[3].z))
    
    v0x_samples+=('x'+str(velocities[0].x))
    v0y_samples+=('x'+str(velocities[0].y))
    v0z_samples+=('x'+str(velocities[0].z))
    
    v1x_samples+=('x'+str(velocities[1].x))
    v1y_samples+=('x'+str(velocities[1].y))
    v1z_samples+=('x'+str(velocities[1].z))
    
    v2x_samples+=('x'+str(velocities[2].x))
    v2y_samples+=('x'+str(velocities[2].y))
    v2z_samples+=('x'+str(velocities[2].z))
    
    v3x_samples+=('x'+str(velocities[3].x))
    v3y_samples+=('x'+str(velocities[3].y))
    v3z_samples+=('x'+str(velocities[3].z))

print('Part 2')

periods = []

periods.append(estimate_period(p0x_samples))
periods.append(estimate_period(p0y_samples))
periods.append(estimate_period(p0z_samples))

periods.append(estimate_period(p1x_samples))
periods.append(estimate_period(p1y_samples))
periods.append(estimate_period(p1z_samples))

periods.append(estimate_period(p2x_samples))
periods.append(estimate_period(p2y_samples))
periods.append(estimate_period(p2z_samples))

periods.append(estimate_period(p3x_samples))
periods.append(estimate_period(p3y_samples))
periods.append(estimate_period(p3z_samples))

periods.append(estimate_period(v0x_samples))
periods.append(estimate_period(v0y_samples))
periods.append(estimate_period(v0z_samples))

periods.append(estimate_period(v1x_samples))
periods.append(estimate_period(v1y_samples))
periods.append(estimate_period(v1z_samples))

periods.append(estimate_period(v2x_samples))
periods.append(estimate_period(v2y_samples))
periods.append(estimate_period(v2z_samples))

periods.append(estimate_period(v3x_samples))
periods.append(estimate_period(v3y_samples))
periods.append(estimate_period(v3z_samples))

print(periods)
# CalculatorSoup can't handle it https://www.calculatorsoup.com/calculators/math/lcm.php
# Only Calculator.net is capable https://www.calculator.net/lcm-calculator.html
unique_numbers = numpy.unique(periods)
# NumPy needs int64 data type to handle it
unique_numbers = numpy.array(unique_numbers, dtype='int64')
print('lcm', numpy.lcm.reduce(unique_numbers))
