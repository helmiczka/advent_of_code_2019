import csv
import sys
from enum import Enum
from classy_intcode import IntcodeMachine


class Direction(Enum):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3


with open(sys.argv[1], newline='') as csvfile:
    data = list(csv.reader(csvfile))

tape = data[0]

visited_panels = dict()
current_pos = (0, 0)
current_direction = Direction.UP
color = 1
halted = False
im = IntcodeMachine(tape)

print('Robot starts')

while not halted:

    # get output from the robot brain
    (color_to_paint, halted) = im.execute_intcode(USER_INPUT=color, pause_after_output=True)
    if not halted:
        (direction, halted) = im.execute_intcode(USER_INPUT=color, pause_after_output=True)

    # paint the current panel
    visited_panels[current_pos] = color_to_paint[0]

    # set new direction
    if int(direction[0]) == 0:
        if current_direction == Direction.UP:
            current_direction = Direction.LEFT
        elif current_direction == Direction.LEFT:
            current_direction = Direction.DOWN
        elif current_direction == Direction.DOWN:
            current_direction = Direction.RIGHT
        elif current_direction == Direction.RIGHT:
            current_direction = Direction.UP
    if int(direction[0]) == 1:
        if current_direction == Direction.UP:
            current_direction = Direction.RIGHT
        elif current_direction == Direction.RIGHT:
            current_direction = Direction.DOWN
        elif current_direction == Direction.DOWN:
            current_direction = Direction.LEFT
        elif current_direction == Direction.LEFT:
            current_direction = Direction.UP
    # print('going',current_direction)

    # move to the new panel
    if current_direction == Direction.UP:
        current_pos = (current_pos[0], current_pos[1] - 1)
    if current_direction == Direction.RIGHT:
        current_pos = (current_pos[0] + 1, current_pos[1])
    if current_direction == Direction.DOWN:
        current_pos = (current_pos[0], current_pos[1] + 1)
    if current_direction == Direction.LEFT:
        current_pos = (current_pos[0] - 1, current_pos[1])

    # get new color from the new panel
    # print('moving to',current_pos)
    if current_pos not in visited_panels:
        color = 0
    else:
        color = visited_panels[current_pos]
    # print('found',color,'there')

print('Robot ends')

print(len(visited_panels))

for y in range(0, 10):
    for x in range(0, 40):
        if (x, y) not in visited_panels:
            print(' ', end='')
        else:
            if visited_panels[(x, y)] == 1:
                print('O', end='')
            else:
                print(' ', end='')
    print()
# print(saved_tape)
